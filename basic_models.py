import numpy as np
import matplotlib.pyplot as plt


class Model:
    def __init__(self):
        self.data = None
        self.name = ''

    def check_fitted_(self):
        if self.data is None:
            raise Exception('The model must be fitted at least once')

    def predict_sample(self, x):
        pass

    def predict(self, X):
        self.check_fitted_()
        return list(map(lambda x: self.predict_sample(x), X))

    def fit(self, data):
        pass

    def plot_2d(self, d0=0, d1=1):
        plt.ylim((-150, 150))
        plt.xlim((-150, 150))
        plt.subplot(121)
        plt.suptitle(self.name)
        self.data.plot_2d(d0, d1)
        plt.xlabel('x position')
        plt.ylabel('y position')
        plt.title('Classes representation')
        X = self.data.X()
        y_hat = self.predict(X)
        plt.subplot(122)
        plt.scatter(X[:, d0], X[:, d1], c=y_hat, alpha=0.5)
        plt.xlabel('x position')
        plt.title('Results')


class LinearModel(Model):
    def __init__(self):
        super().__init__()
        self.B_hat = None
        self.name = 'Least Square classification'

    def predict_sample(self, x):
        self.check_fitted_()
        return {True: 1, False: 0}[(x.T @ self.B_hat)[0] > 0.5]

    def fit(self, data):
        self.data = data
        y = data.y()
        X = data.X()
        A = X.T @ X
        if np.linalg.det(A) < 0.0001:
            raise Exception('Matrix is singular')
        A = np.linalg.inv(A) @ X.T
        self.B_hat = A @ y


class NearestNeighbors(Model):
    def __init__(self, k=8):
        super().__init__()
        self.k = k
        self.name = 'Nearest Neighbors classification'

    def predict_sample(self, x):
        self.check_fitted_()
        x = x.reshape((1, self.data.dim))
        y = self.data.y()
        distances = np.ones((len(y), 1)) @ x - self.data.X()
        distances = np.linalg.norm(distances, axis=1)
        neighbors = np.argpartition(distances, self.k)[:self.k]
        classes = y[neighbors]
        raw_result = np.sum(classes) / self.k
        prediction = classes.flat[np.abs(classes - raw_result).argmin()]
        return prediction

    def fit(self, data):
        self.data = data


def scores(ds, models):
    results = np.zeros((len(models), 2, ds.nb_class))
    for i, model in enumerate(models):
        Y_train = ds.y()[:, 0]
        Y_test = model.predict(ds.X())
        labels = np.linspace(0, 1, ds.nb_class)

        def zip_class(train, test):
            for a, b in zip(train, test):
                k = -1
                for j, val in enumerate(labels):
                    if val == a:
                        k = j
                        break
                if k == -1:
                    raise Exception('Could not find this label')
                yield k, a, b

        for k, val_train, val_test in zip_class(Y_train, Y_test):
            results[i][0][k] += int(val_test == val_train)
            results[i][1][k] += int(val_test != val_train)
    return results


def plot_classification_results(ds, models):
    def name_generator(n):
        for i in range(n):
            yield 'Class ' + str(i + 1)

    plt.title('Classification results')

    def plot_simple(tuple):
        good, bad = tuple
        ind = np.arange(ds.nb_class)
        width = 0.35
        p1 = plt.bar(ind, good, width)
        p2 = plt.bar(ind, bad, width, bottom=good)
        plt.xticks(ind, list(name_generator(ds.nb_class)))
        plt.legend((p1[0], p2[0]), ('Well classified points', 'Misclassified points'))
        plt.show()

    scs = scores(ds, models)
    if len(scs) <= 3:
        for t in scs:
            plot_simple(t)
        return

    test = scs[:, 0] / (scs[:, 0] + scs[:, 1])
    plt.ylabel('Success rate')
    plt.xlabel('Parameter')
    plt.plot(test)
    plt.show()
