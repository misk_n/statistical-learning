import numpy as np
import matplotlib.pyplot as plt


class NormalDataSet:

    def __init__(self, nb_class=2, dim=2, items=8, scale=4.0):
        self.nb_class = nb_class
        self.dim = dim
        self.items = items
        self.scale = scale
        self.data = np.zeros((nb_class, items, dim))
        self.centers = np.random.normal(loc=0, scale=60.0, size=(nb_class, dim))
        self.labels = np.linspace(0, 1, self.nb_class)
        for k in range(nb_class):
            for cl, pos in enumerate(self.centers[k]):
                self.data[k, :, cl] = np.random.normal(loc=pos, scale=scale, size=(1, items))

    def y(self):
        line = self.labels.reshape(self.nb_class, 1)
        y = line @ np.ones((1, self.items))
        return y.reshape((self.items * self.nb_class, 1))

    def X(self):
        return self.data.reshape(self.data.shape[0] * self.data.shape[1], self.data.shape[2])

    def plot_2d(self, d0=0, d1=1):
        colors = self.y()
        x = self.data.reshape((self.nb_class * self.items, self.dim))[:, d0]
        y = self.data.reshape((self.nb_class * self.items, self.dim))[:, d1]
        plt.scatter(x, y, c=colors[:, 0], alpha=0.5)
